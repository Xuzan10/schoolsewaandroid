package com.schoolapp.rashim12000.schoolims;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieManager;
import java.util.ArrayList;

public class MarksheetActivity extends AppCompatActivity {
    private SessionManager session;
    private ProgressDialog pDialog;
    private String[] subject;
    private String[] DataSub;
    private String[] DataObt;

    TableLayout tableview;
    public String st_id;
    public String ex_id;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marksheet);
       getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        sharedPreferences = getSharedPreferences("studentdetail",0);
        st_id = sharedPreferences.getString("STUDENT_ID","");

        CookieManager cookieManage = new CookieManager();
        cookieManage.getCookieStore();
//        CookieHandler.setDefault(cookieManage);
        // session manager
        session = new SessionManager(getApplicationContext());
        session.setLogin(true);

//        Bundle extras = getIntent().getExtras();
        Intent intent = getIntent();
        if (intent != null) {
            ex_id = intent.getStringExtra("EXAM_ID");
        } else {
            Toast.makeText(this, "data not received", Toast.LENGTH_SHORT).show();
        }
        tableview = (TableLayout) findViewById(R.id.tableViewMarksheet);

        getMarksheetJson();
    }

    public void init() {

        TableRow tbrow0 = new TableRow(this);
        TextView tv0 = new TextView(this);
        tv0.setText(" Subject ");
//        tv0.setTextColor(Color.WHITE);
        tbrow0.addView(tv0);
//        TextView tv1 = new TextView(this);
//        tv1.setText(" Pass Marks ");
//        tv1.setTextColor(Color.WHITE);
//        tbrow0.addView(tv1);
//        TextView tv2 = new TextView(this);
//        tv2.setText(" Full Marks ");
//        tv2.setTextColor(Color.WHITE);
//        tbrow0.addView(tv2);
        TextView tv3 = new TextView(this);
        tv3.setText(" Obtained Marks ");
//        tv3.setTextColor(Color.WHITE);
        tbrow0.addView(tv3);
        tableview.addView(tbrow0);
    }

    private void getMarksheetJson() {
        String tag_string_req = "req_data";

        pDialog.setMessage("Fetching data ...");
        showDialog();
        String url = AppConfig.URL_GET_MARKSHEET + "?exam_id=" + ex_id + "&student_id=" + st_id;
        Log.i("marksheet url", url);
        JsonArrayRequest req = new JsonArrayRequest(Request.Method.GET,
                url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                try {
                    hideDialog();

//                        JSONObject jObj = new JSONObject(response);
                    Log.i("tagjmarkhseetresponse", "[" + response.toString() + "]");
//                    JSONObject jObj = new JSONObject(response);
////
////                    JSONObject jsonObject = new JSONObject("student");
//                    JSONArray result = jObj.getJSONArray("student");
                    ArrayList<Marksheet> listdata = new ArrayList<>();
                    subject = new String[2];
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject jo = response.getJSONObject(i);
                        Marksheet marksheet = new Marksheet();
                        marksheet.setExamName(jo.getString(AppConfig.TAG_MARKSHEET_EXAM));
                        marksheet.setStudnetName(jo.getString(AppConfig.TAG_MARKSHEET_STUDENT));
                        marksheet.setClassName(jo.getString(AppConfig.TAG_MARKSHEET_CLASS));
                        marksheet.setHasFile(Boolean.parseBoolean(jo.getString(AppConfig.TAG_MARKSHEET_HAS_FILE)));
                        Log.i("true or false", (jo.getString(AppConfig.TAG_MARKSHEET_HAS_FILE)));
                        Log.i("boolena true or false", String.valueOf(Boolean.parseBoolean(jo.getString(AppConfig.TAG_MARKSHEET_HAS_FILE))));

                        if ((Boolean.parseBoolean(jo.getString(AppConfig.TAG_MARKSHEET_HAS_FILE)))) {

                            Toast.makeText(MarksheetActivity.this, "View Image", Toast.LENGTH_SHORT).show();
                            marksheet.setFileName((jo.getString(AppConfig.TAG_MARKSHEET_FILE_NAME)));
                            listdata.add(marksheet);

//                            showMarksheetList(listdata);

                        } else {


                            String sub = (jo.getString(AppConfig.TAG_MARKSHEET_SUBJECTS));
                            String obt = (jo.getString(AppConfig.TAG_MARKSHEET_OBT_MARKS));

                            Log.i("sub sub", sub);
                            subject[0] = sub;
                            subject[1] = obt;
                            subject[0] = subject[0].replaceAll("\\[", "").replaceAll("\\]", "");
                            subject[1] = subject[1].replaceAll("\\[", "").replaceAll("\\]", "");
//                        subject[0]=subject[0].replaceAll(","," ");
                            DataSub = subject[0].split(",");
                            DataObt = subject[1].split(",");
//


                            marksheet.setSubjects(DataSub);
                            marksheet.setObtMarks(DataObt);

                            listdata.add(marksheet);

                            showMarksheetList(listdata);
                        }

                    }
                } catch (JSONException e) {
                    hideDialog();
                    Toast.makeText(MarksheetActivity.this,"Marksheet not available",Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideDialog();
                        Toast.makeText(MarksheetActivity.this,"Marksheet not available",Toast.LENGTH_LONG).show();

//                        Log.i("error", error.getMessage());
                    }
                }) {
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req, tag_string_req);
    }

    private void showMarksheetList(ArrayList<Marksheet> listdata) {
        init();
        Log.i("data length", "length" + DataSub.length);
        for (int k = 0; k < DataSub.length; k++) {
            Log.i("data data", "datasub =" + DataSub[k]);
            Log.i("data data", "datasub =" + DataObt[k]);

            TableRow tbrow = new TableRow(this);
            TextView t1v = new TextView(this);
            t1v.setText(DataSub[k]);
//            t1v.setTextColor(Color.WHITE);
            t1v.setGravity(Gravity.CENTER);
            tbrow.addView(t1v);
//            TextView t2v = new TextView(this);
//            t2v.setText(subFm);
//            t2v.setTextColor(Color.WHITE);
//            t2v.setGravity(Gravity.CENTER);
//            tbrow.addView(t2v);
//            TextView t3v = new TextView(this);
//            t3v.setText(subPm);
//            t3v.setTextColor(Color.WHITE);
//            t3v.setGravity(Gravity.CENTER);
//            tbrow.addView(t3v);
            TextView t4v = new TextView(this);
            t4v.setText(DataObt[k]);
//            t4v.setTextColor(Color.WHITE);
            t4v.setGravity(Gravity.CENTER);
            tbrow.addView(t4v);
            tableview.addView(tbrow);
        }

    }

    @Override
    public void onBackPressed() {

        startActivity(new Intent(MarksheetActivity.this,MarksheetActivityFragment.class));
        finish();

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            onBackPressed();
        }
        return true;
    }
}
