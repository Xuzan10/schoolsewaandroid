package com.schoolapp.rashim12000.schoolims.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.schoolapp.rashim12000.schoolims.ClassRoutine;
import com.schoolapp.rashim12000.schoolims.Notice;
import com.schoolapp.rashim12000.schoolims.NoticeStudent;
import com.schoolapp.rashim12000.schoolims.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Rashim12000 on 10/31/2017.
 */

public class ClassRoutineCardAdapter extends RecyclerView.Adapter<ClassRoutineCardAdapter.ClassRoutineViewHolder> {

    ArrayList<ClassRoutine> items;
    private int itemLayout;
    Typeface typeface;


    public ClassRoutineCardAdapter(ArrayList<ClassRoutine> items, int itemLayout) {
        this.items = items;
        this.itemLayout = itemLayout;
    }

    @Override
    public ClassRoutineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
         typeface = Typeface.createFromAsset(v.getContext().getAssets(),"font/Amarante.ttf");
        return new ClassRoutineViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ClassRoutineViewHolder holder, int position) {

        ClassRoutine item = items.get(position);
        String str = "";
        if (Integer.parseInt(item.getPeriodName()) == 1) {
            str = "st";
        } else if (Integer.parseInt(item.getPeriodName()) == 2) {
            str="nd";

        } else if (Integer.parseInt(item.getPeriodName()) == 3) {
            str="rd";
        } else {
            str = "th";
        }
        holder.tv_period_name.setText(item.getPeriodName() + str + " period");
        holder.tv_period_subject.setText( item.getPeriodSubject());
        holder.tv_period_teacher.setText(item.getPeriodTeacher());
        holder.tv_period_start_time.setText(item.getPeriodStartTime());
        holder.tv_period_end_time.setText(" - "+ item.getPeriodEndTime());
        holder.tv_period_teacher.setTypeface(typeface);
        holder.tv_period_subject.setTypeface(typeface);
        holder.tv_period_start_time.setTypeface(typeface);
        holder.tv_period_end_time.setTypeface(typeface);

        Random r = new Random();
        int red=r.nextInt(255 - 0 + 1)+0;
        int green=r.nextInt(255 - 0 + 1)+0;
        int blue=r.nextInt(255 - 0 + 1)+0;

        GradientDrawable draw = new GradientDrawable();
        draw.setShape(GradientDrawable.RECTANGLE);
        draw.setColor(Color.rgb(red,green,blue));
        holder.colorstrip.setBackground(draw);

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ClassRoutineViewHolder extends RecyclerView.ViewHolder {

        public static TextView tv_period_name;
        public static TextView tv_period_start_time;
        public static TextView tv_period_end_time;

        public static TextView tv_period_subject;
        public static TextView tv_period_teacher;
        Button colorstrip;


        public ClassRoutineViewHolder(View v) {
            super(v);

            tv_period_name = (TextView) v.findViewById(R.id.period_name);
            tv_period_start_time = (TextView) v.findViewById(R.id.period_start_time);
            tv_period_subject = (TextView) v.findViewById(R.id.period_subject);
            tv_period_end_time = (TextView) v.findViewById(R.id.period_end_time);

            tv_period_teacher = (TextView) v.findViewById(R.id.period_teacher);
            colorstrip = (Button) v.findViewById(R.id.colorstrip);


        }
    }

}