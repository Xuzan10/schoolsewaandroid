package com.schoolapp.rashim12000.schoolims;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.schoolapp.rashim12000.schoolims.ClassRoutineFragment.Friday;
import com.schoolapp.rashim12000.schoolims.ClassRoutineFragment.Monday;
import com.schoolapp.rashim12000.schoolims.ClassRoutineFragment.Saturday;
import com.schoolapp.rashim12000.schoolims.ClassRoutineFragment.Sunday;
import com.schoolapp.rashim12000.schoolims.ClassRoutineFragment.Thursday;
import com.schoolapp.rashim12000.schoolims.ClassRoutineFragment.Tuesday;
import com.schoolapp.rashim12000.schoolims.ClassRoutineFragment.Wednesday;
import com.schoolapp.rashim12000.schoolims.NoticeFragment.AllNotice;
import com.schoolapp.rashim12000.schoolims.NoticeFragment.StudentNotice;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rashim12000 on 10/31/2017.
 */

public class ScrollableClassRoutineTabsActivity extends AppCompatActivity {


    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_routine_scrollable_tabs);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new Sunday(), "Sunday");
        adapter.addFrag(new Monday(), "Monday");
        adapter.addFrag(new Tuesday(), "Tuesday");
        adapter.addFrag(new Wednesday(), "Wednesday");
        adapter.addFrag(new Thursday(), "Thursday");
        adapter.addFrag(new Friday(), "Friday");
        adapter.addFrag(new Saturday(), "Saturday");

        viewPager.setAdapter(adapter);
    }
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    Sunday tab1 = new Sunday();
                    return tab1;
                case 1:
                    Monday tab2 = new Monday();
                    return tab2;
                case 3:
                    Tuesday tab3 = new Tuesday();
                    return tab3;
                case 4:
                    Wednesday tab4 = new Wednesday();
                    return tab4;
                case 5:
                    Thursday tab5 = new Thursday();
                    return tab5;
                case 6:
                    Friday tab6 = new Friday();
                    return tab6;


                default:
                    Saturday tab7 =new Saturday();
                    return tab7;
            }


        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}


