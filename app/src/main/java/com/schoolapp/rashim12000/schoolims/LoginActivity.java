package com.schoolapp.rashim12000.schoolims;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.messaging.FirebaseMessaging;
import com.tapadoo.alerter.Alerter;


import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class LoginActivity extends Activity {
    private static final String TAG = LoginActivity.class.getSimpleName();
    private Button btnLogin;
    private EditText inputEmail;
    private EditText inputPassword;
    private ProgressDialog pDialog;
    private SessionManager session;
    private SQLiteHandler db;
    String CSRFTOKEN;
    private BroadcastReceiver mRegistrationBroadcastReceiver;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        //  btnLinkToRegister = (Button) findViewById(R.id.btnLinkToRegisterScreen);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        // Session manager
        CookieManager cookieManage = new CookieManager();
        CookieHandler.setDefault(cookieManage);


        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());


        session = new SessionManager(getApplicationContext());
        getCSRFVolley(new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                CSRFTOKEN = result;
                Log.d(TAG, "before CSRFTOKEN received =" + CSRFTOKEN);
            }
        });


        FirebaseMessaging.getInstance().subscribeToTopic("global");

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(AppConfig.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(AppConfig.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(AppConfig.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");
                    Log.i("msg",message);
                    Alerter.create(LoginActivity.this)
                            .setTitle("Notice!!!")
                            .setText(message)
                            .setBackgroundColorRes(R.color.view_bg)
                            .enableSwipeToDismiss()
                            .enableInfiniteDuration(true)
                            .show();


                }
            }
        };





        // Check if user is already logged in or not
        /*if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity

            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
            startActivity(intent);
            finish();
        }*/

        // Login button Click Event
        btnLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                final String email = inputEmail.getText().toString().trim();
                final String password = inputPassword.getText().toString().trim();
                showDialog();
                pDialog.setMessage("Logging in ...");
                // Check for empty data in the form
                if (!email.isEmpty() && !password.isEmpty()) {

                    FirebaseAuth.getInstance().signInWithEmailAndPassword(
                            email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Log.d(TAG, "firebase auth success");



                                checkLogin(email, password);
                            } else {
                                Log.d(TAG, "firebase auth failed");
                                Toast.makeText(LoginActivity.this,"Unsuccessful",Toast.LENGTH_SHORT).show();

                            }
                        }
                    });
                    // login user

                } else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please enter the credentials!", Toast.LENGTH_LONG)
                            .show();
                }
            }

        });

        // Link to Register Screen
        /*btnLinkToRegister.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),
                        RegisterActivity.class);
                startActivity(i);
                finish();
            }
        });*/

    }

    /**
     * function to verify login details in mysql db
     */
    private void checkLogin(final String email, final String password) {

        // Authenticate with Firebase and subscribe to updates

        // Tag used to cancel the request
        String tag_string_req = "req_login";




////        final String token = SharedPrefManager.getInstance(this).getDeviceToken();
//        final String token = FirebaseInstanceId.getInstance().getToken();
//        Log.d("token", "token " + token);
////        final String token =FirebaseInstanceId.getInstance().getToken();
////                Log.i(TAG,"token ="+token );
//        if (token == null) {
//            pDialog.dismiss();
//            Toast.makeText(this, "Token not generated", Toast.LENGTH_LONG).show();
//            return;
//        }


//        getCSRF();
//        getCSRFVolley();




        Log.i(TAG, "url login = " + AppConfig.URL_LOGIN);
        StringRequest strReq = new StringRequest(Method.POST,
                AppConfig.URL_LOGIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                //  Log.d(TAG, "Login Response: " + response.toString());
                hideDialog();

                try {
//                    Log.i("tagconvertstr", "[" + response + "]");
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        // user successfully logged in
                        // Create login session
                        session.setLogin(true);

                        // Now store the user in SQLite
//                String user_id = jObj.getString("user_id");

                        JSONObject user = jObj.getJSONObject("user");
                        String user_id = user.getString("user_id");
                        String role_id = user.getString("role_id");
                        String email = user.getString("email");
//
                        JSONObject guardian = jObj.getJSONObject("guardian");
                        String guardian_id = guardian.getString("guardian_id");
                        String guardian_name = guardian.getString("guardian_name");


//                       /* String created_at = user
//                                .getString("created_at");*/
//
                        db.deletedatafromTable();
//                        // Inserting row in users table
                        db.addUser(user_id, role_id, email, guardian_id, guardian_name);

//                        // Launch main activity
                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
//                        Intent intent = new Intent(LoginActivity.this, TestActivity.class);
//                        startActivity(intent);
//                        finish();
                        Toast.makeText(LoginActivity.this, " Login successful", Toast.LENGTH_SHORT).show();
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("message");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //  Log.e(TAG, "Login Error: " + error.getMessage());
                if (error != null && error.getMessage() != null) {
                    Toast.makeText(getApplicationContext(), "after crsf error VOLLEY " + error.getMessage(), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
                }
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                SharedPreferences pref = getApplicationContext().getSharedPreferences(AppConfig.SHARED_PREF, 0);
                String regId = pref.getString("regId", null);

                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("password", password);
                params.put("token", regId);
                params.put("_token", CSRFTOKEN);

                return checkParams(params);
            }

            private Map<String, String> checkParams(Map<String, String> map) {
                Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry<String, String> pairs = (Map.Entry<String, String>) it.next();
                    if (pairs.getValue() == null) {
                        map.put(pairs.getKey(), "");
                    }
                }
                return map;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void getCSRFVolley(final VolleyCallback callback) {
        String tag_string_req = "req_csrf";
//         String _token;
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, AppConfig.URL_GET_TOKEN,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
//                            JSONObject obj = response.getJSONObject("_token");
                            CSRFTOKEN = response.getString("_token");
//                            Log.i("_token", CSRFTOKEN);
                            callback.onSuccessResponse(CSRFTOKEN);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null && error.getMessage() != null) {
                            Toast.makeText(getApplicationContext(), "error VOLLEY " + error.getMessage(), Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
                        }
                    }
                }) {
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req, tag_string_req);
        hideDialog();
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(AppConfig.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        Log.e(TAG, "Firebase reg id: " + regId);


    }


    @Override
    protected void onResume() {
        super.onResume();
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(AppConfig.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(AppConfig.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }





}

