package com.schoolapp.rashim12000.schoolims;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by DEll on 11/24/2017.
 */

public class DatabaseHelperDriver extends SQLiteOpenHelper {

    static String name = "driverdb";
    static int version = 1;

    String createDriverTable = "CREATE TABLE if not exists \"drivers\" ( `id` INTEGER PRIMARY KEY AUTOINCREMENT, `driver_name` TEXT, `driver_email` TEXT, `driver_mobile` TEXT, `student_id` INTEGER )";


    public DatabaseHelperDriver(Context context) {
        super(context, name, null, version);
        getWritableDatabase().execSQL(createDriverTable);
    }

    public void insertDriverDetails(ContentValues cv){
        getWritableDatabase().insert("drivers",null,cv);
    }

    public ArrayList<DriverInfo> getDriverList(){

        String sql = "Select * from drivers";
        Cursor cursor = getWritableDatabase().rawQuery(sql,null);
        ArrayList<DriverInfo> list = new ArrayList<>();
        while (cursor.moveToNext()){
            DriverInfo info = new DriverInfo();
            info.driver_name = cursor.getString(cursor.getColumnIndex("driver_name"));
            info.driver_email = cursor.getString(cursor.getColumnIndex("driver_email"));
            info.driver_mobile = cursor.getString(cursor.getColumnIndex("driver_mobile"));
            info.student_id = cursor.getString(cursor.getColumnIndex("student_id"));
            list.add(info);
        }
        cursor.close();
        return list;
    }

    public DriverInfo getDriverInfo(String student_id) {
        String sql = "Select * from drivers where student_id=" + student_id ;
        Cursor cursor = getWritableDatabase().rawQuery(sql, null);
        DriverInfo info = new DriverInfo();
        while (cursor.moveToNext()) {

            info.driver_name = cursor.getString(cursor.getColumnIndex("driver_name"));
            info.driver_email = cursor.getString(cursor.getColumnIndex("driver_email"));
            info.driver_mobile = cursor.getString(cursor.getColumnIndex("driver_mobile"));
            info.student_id = cursor.getString(cursor.getColumnIndex("student_id"));

        }
        cursor.close();
        return info;
    }

    public void deletedatafromTable(){
        String sql = "Delete from drivers ";
        getWritableDatabase().execSQL(sql);
    }







    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
