package com.schoolapp.rashim12000.schoolims;

/**
 * Created by Rashim12000 on 11/5/2017.
 */

public class ClassRoutine {
    String periodName;
    String periodStartTime;
    String periodEndTime;

    String periodTeacher;
    String periodSubject;

    public ClassRoutine() {
    }

    public ClassRoutine(String periodName, String periodStartTime,String periodEndTime, String periodTeacher, String periodSubject) {
        this.periodName = periodName;
        this.periodStartTime = periodStartTime;
        this.periodEndTime = periodEndTime;
        this.periodTeacher = periodTeacher;
        this.periodSubject = periodSubject;
    }

    public String getPeriodName() {
        return periodName;
    }

    public void setPeriodName(String periodName) {
        this.periodName = periodName;
    }

    public String getPeriodStartTime() {
        return periodStartTime;
    }

    public void setPeriodStartTime(String periodStartTime) {
        this.periodStartTime = periodStartTime;
    }

    public String getPeriodEndTime() {
        return periodEndTime;
    }

    public void setPeriodEndTime(String periodEndTime) {
        this.periodEndTime = periodEndTime;
    }

    public String getPeriodTeacher() {
        return periodTeacher;
    }

    public void setPeriodTeacher(String periodTeacher) {
        this.periodTeacher = periodTeacher;
    }

    public String getPeriodSubject() {
        return periodSubject;
    }

    public void setPeriodSubject(String periodSubject) {
        this.periodSubject = periodSubject;
    }

}
