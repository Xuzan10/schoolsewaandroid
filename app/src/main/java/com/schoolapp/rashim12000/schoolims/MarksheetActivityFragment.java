package com.schoolapp.rashim12000.schoolims;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.schoolapp.rashim12000.schoolims.Adapter.ExamCardAdapter;
import com.schoolapp.rashim12000.schoolims.Adapter.RecyclerItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieManager;
import java.util.ArrayList;

/**
 * Created by Rashim12000 on 11/1/2017.
 */

public class MarksheetActivityFragment extends AppCompatActivity {
    private SharedPreferences sharedPreferences;

    public MarksheetActivityFragment() {
    }

    private ProgressDialog pDialog;

    protected RecyclerView mRecyclerView;
    private String st_id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_marksheet);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        CookieManager cookieManage = new CookieManager();
        cookieManage.getCookieStore();
        sharedPreferences = getSharedPreferences("studentdetail",0);
        st_id = sharedPreferences.getString("STUDENT_ID","");
        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        Intent intent = getIntent();
        if (intent != null) {
        } else {
            Toast.makeText(this, "data not received", Toast.LENGTH_SHORT).show();
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.cardlist_exam);

        getExamJson();


    }

    private void getExamJson() {
        String tag_string_req = "req_data";

        pDialog.setMessage("Fetching data ...");
        showDialog();

        JsonArrayRequest req = new JsonArrayRequest(Request.Method.GET,
                AppConfig.URL_GET_EXAM, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                try {
                    hideDialog();

//                        JSONObject jObj = new JSONObject(response);
                    Log.i("tagconvertresponse", "[" + response.toString() + "]");
//                    JSONObject jObj = new JSONObject(response);
////
////                    JSONObject jsonObject = new JSONObject("student");
//                    JSONArray result = jObj.getJSONArray("student");
                    ArrayList<Exam> listdata = new ArrayList<>();
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject jo = response.getJSONObject(i);
                        Exam exams = new Exam();
                        exams.setExamName(jo.getString(AppConfig.TAG_EXAM_NAME));
                        exams.setExamId(jo.getString(AppConfig.TAG_EXAM_ID));


                        listdata.add(exams);
                        showFeedback(listdata);
                    }
                } catch (JSONException e) {
                    hideDialog();
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideDialog();
//                        Log.i("error", error.getMessage());
                    }
                }) {
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req, tag_string_req);
    }

    private void showFeedback(final ArrayList<Exam> listdata) {

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(MarksheetActivityFragment.this));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(new ExamCardAdapter(listdata));

        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(MarksheetActivityFragment.this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                String exam_id = listdata.get(position).getExamId();
                String exam_name = listdata.get(position).getExamName();
                Intent i = new Intent(MarksheetActivityFragment.this, MarksheetDisplayActivity.class);
                i.putExtra("EXAM_ID", exam_id);
                i.putExtra("STUDENT_ID", st_id);
                startActivity(i);
            }
        }));


    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(MarksheetActivityFragment.this, StudentOptionActivity.class));
        finish();
    }
}

