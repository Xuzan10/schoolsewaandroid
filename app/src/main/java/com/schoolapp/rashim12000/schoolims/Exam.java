package com.schoolapp.rashim12000.schoolims;

import android.widget.ImageView;

/**
 * Created by Rashim12000 on 10/31/2017.
 */

public class Exam {
    String examName;
    String examId;


    public Exam(){

    }

    public Exam(String examName, String examId) {
        this.examName = examName;
        this.examId = examId;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }
}
