package com.schoolapp.rashim12000.schoolims.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.schoolapp.rashim12000.schoolims.ExamRoutine;
import com.schoolapp.rashim12000.schoolims.R;
import com.schoolapp.rashim12000.schoolims.Subjects;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DEll on 11/27/2017.
 */

public class ExamRoutineAdapter extends RecyclerView.Adapter<ExamRoutineAdapter.ExamRoutineViewHolder> {
    ArrayList<ExamRoutine> items;
    Context context;

    public ExamRoutineAdapter(ArrayList<ExamRoutine> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public ExamRoutineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.cardview_exam_routine,parent,false);
        return new ExamRoutineViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ExamRoutineViewHolder holder, int position) {

        ExamRoutine routine = items.get(position);
        holder.exam_name.setText(routine.getExam_name());
        holder.class_name.setText("Class : " +routine.getClass_name());


    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ExamRoutineViewHolder extends RecyclerView.ViewHolder{
        public TextView class_name,exam_name;

        public ExamRoutineViewHolder(View v) {
            super(v);

           class_name = (TextView) v.findViewById(R.id.class_name);
           exam_name = (TextView) v.findViewById(R.id.exam_name);

        }
    }
}
