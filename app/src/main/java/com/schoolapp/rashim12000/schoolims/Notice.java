package com.schoolapp.rashim12000.schoolims;

import android.widget.ImageView;

/**
 * Created by Rashim12000 on 10/31/2017.
 */

public class Notice {
    String noticeTitle;
    String noticeBody;
    ImageView noticeImage;

    public  Notice(){

    }

    public Notice(String noticeTitle, String noticeBody, ImageView noticeImage) {
        this.noticeTitle = noticeTitle;
        this.noticeBody = noticeBody;
        this.noticeImage = noticeImage;
    }

    public String getNoticeTitle() {
        return noticeTitle;
    }

    public void setNoticeTitle(String noticeTitle) {
        this.noticeTitle = noticeTitle;
    }

    public String getNoticeBody() {
        return noticeBody;
    }

    public void setNoticeBody(String noticeBody) {
        this.noticeBody = noticeBody;
    }

    public ImageView getNoticeImage() {
        return noticeImage;
    }

    public void setNoticeImage(ImageView noticeImage) {
        this.noticeImage = noticeImage;
    }
}
