package com.schoolapp.rashim12000.schoolims;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieManager;
import java.util.HashMap;
import java.util.Map;

public class FeedbackActivity extends AppCompatActivity {

    EditText et_feedback;
    Button btn_send_feedback;
    private SessionManager session;
    private ProgressDialog pDialog;
    private SQLiteHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own feedback", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        CookieManager cookieManage = new CookieManager();
        cookieManage.getCookieStore();
//        CookieHandler.setDefault(cookieManage);
        // session manager
        db = new SQLiteHandler(getApplicationContext());
//        session = new SessionManager(getApplicationContext());
//
//        if (!session.isLoggedIn()) {
//            logoutUser();
//        }

        et_feedback = (EditText) findViewById(R.id.feedback);
        btn_send_feedback = (Button) findViewById(R.id.send_feedback);
        btn_send_feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String st_feedabck = et_feedback.getText().toString();
                sendfeedback(st_feedabck);
            }
        });
    }

//    private void logoutUser() {
//        session.setLogin(false);
//        db.deleteUsers();
//        // Launching the login activity
//        Intent intent = new Intent(FeedbackActivity.this, LoginActivity.class);
//        startActivity(intent);
//        finish();
//    }

    private void sendfeedback(final String st_feedabck) {
        String tag_string_req = "send_feedback";
        Log.i("st_feedback",st_feedabck);
//
        pDialog.setMessage("Sending Feedback ...");
        showDialog();
//
        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST,
                AppConfig.URL_SEND_FEEDBACK, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                hideDialog();

                try {
//                    Log.i("tagconvertstrhome", "[" + response + "]");
//                    JSONObject jObj = new JSONObject(response);
                    boolean error = response.getBoolean("error");
                    Log.i("feedbackactivity","error = "+error);

                    // Check for error node in json
                    if (!error) {

//                      finish();
                        Toast.makeText(FeedbackActivity.this, "Feedback Sent", Toast.LENGTH_SHORT).show();

                    } else {
                        // Error in login. Get the error message
                        String errorMsg = response.getString("message");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    hideDialog();
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //  Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("feedback", st_feedabck);
                return params;
            }

        };

        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
