package com.schoolapp.rashim12000.schoolims.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.schoolapp.rashim12000.schoolims.R;
import com.schoolapp.rashim12000.schoolims.Subjects;

import java.util.ArrayList;

/**
 * Created by DEll on 11/27/2017.
 */

public class ExamSubjectAdapter extends RecyclerView.Adapter<ExamSubjectAdapter.ExamSubjectViewHolder> {
    private ArrayList<Subjects> items;
    private Context context;

    public ExamSubjectAdapter(ArrayList<Subjects> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public ExamSubjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.cardview_exam_subject,parent,false);

        return new ExamSubjectViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ExamSubjectViewHolder holder, int position) {

        Subjects subjects = items.get(position);
        holder.sn.setText(subjects.getSn());
        holder.subject_name.setText(subjects.getSubject_name());
        holder.exam_date.setText(subjects.getExam_date());
        holder.day.setText(subjects.getDay());


    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ExamSubjectViewHolder extends RecyclerView.ViewHolder{

        TextView sn,subject_name,exam_date,day;

        public ExamSubjectViewHolder(View itemView) {
            super(itemView);
            sn = (TextView) itemView.findViewById(R.id.sn);
            subject_name = (TextView) itemView.findViewById(R.id.subject_name);
            exam_date = (TextView) itemView.findViewById(R.id.exam_date);
            day = (TextView) itemView.findViewById(R.id.day);
        }
    }
}
