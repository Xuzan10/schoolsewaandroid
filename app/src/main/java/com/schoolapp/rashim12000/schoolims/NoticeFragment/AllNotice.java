package com.schoolapp.rashim12000.schoolims.NoticeFragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.schoolapp.rashim12000.schoolims.Adapter.NoticeCardAdapter;
import com.schoolapp.rashim12000.schoolims.AppConfig;
import com.schoolapp.rashim12000.schoolims.AppController;
import com.schoolapp.rashim12000.schoolims.Notice;
import com.schoolapp.rashim12000.schoolims.R;
import com.schoolapp.rashim12000.schoolims.Students;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieManager;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Rashim12000 on 10/31/2017.
 */

public class AllNotice extends Fragment {

    public AllNotice(){}

    private ProgressDialog pDialog;

    protected RecyclerView mRecyclerView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_allnotice, container, false);
        CookieManager cookieManage = new CookieManager();
        cookieManage.getCookieStore();
        // Progress dialog
        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);
        getNoticeJson();


        mRecyclerView = (RecyclerView) v.findViewById(R.id.cardlist_allnotice);




        return  v;
    }


    public void getNoticeJson() {

        String tag_string_req = "req_data";

        pDialog.setMessage("Fetching data ...");
        showDialog();

        JsonArrayRequest req = new JsonArrayRequest(Request.Method.GET,
                AppConfig.URL_GET_ALL_NOTICE, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                try {
                    hideDialog();

//                        JSONObject jObj = new JSONObject(response);
                    Log.i("tagconvertresponse", "[" + response.toString() + "]");
//                    JSONObject jObj = new JSONObject(response);
////
////                    JSONObject jsonObject = new JSONObject("student");
//                    JSONArray result = jObj.getJSONArray("student");
                    ArrayList<Notice> listdata = new ArrayList<>();
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject jo = response.getJSONObject(i);
                        Notice notice = new Notice();
                        notice.setNoticeTitle(jo.getString(AppConfig.TAG_NOTICE_TITLE));
                        notice.setNoticeBody(jo.getString(AppConfig.TAG_NOTICE_BODY));
//                        notice.setStudentClass(jo.getString(AppConfig.TAG_CID_S));


                        listdata.add(notice);

                    }
                    Collections.reverse(listdata);
                    showNotice(listdata);
                } catch (JSONException e) {
                    hideDialog();
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideDialog();
//                        Log.i("error", error.getMessage());
                    }
                }) {
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req, tag_string_req);


    }

    private void showNotice(ArrayList<Notice> listdata) {
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mRecyclerView.setAdapter(new NoticeCardAdapter(listdata, R.layout.cardview_notice));



//        mRecyclerView.addOnItemTouchListener(
//                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(View view, int position) {
//                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + n));
//                        startActivity(intent);
//
//                    }
//                })
//        );
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


}
