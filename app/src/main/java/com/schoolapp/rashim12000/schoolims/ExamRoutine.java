package com.schoolapp.rashim12000.schoolims;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DEll on 11/27/2017.
 */

public class ExamRoutine  {
    private String class_id,exam_number,class_name,exam_name;



    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getExam_number() {
        return exam_number;
    }

    public void setExam_number(String exam_number) {
        this.exam_number = exam_number;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getExam_name() {
        return exam_name;
    }

    public void setExam_name(String exam_name) {
        this.exam_name = exam_name;
    }



    public ExamRoutine() {

    }

    public ExamRoutine(String class_id, String exam_number, String class_name, String exam_name) {

        this.class_id = class_id;
        this.exam_number = exam_number;
        this.class_name = class_name;
        this.exam_name = exam_name;
    }
}
