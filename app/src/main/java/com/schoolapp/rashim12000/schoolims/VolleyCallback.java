package com.schoolapp.rashim12000.schoolims;

/**
 * Created by Rashim12000 on 10/21/2017.
 */

public interface VolleyCallback {
    void onSuccessResponse(String result);
}
