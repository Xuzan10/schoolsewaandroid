package com.schoolapp.rashim12000.schoolims;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.schoolapp.rashim12000.schoolims.Adapter.ExamCardAdapter;
import com.schoolapp.rashim12000.schoolims.Adapter.RecyclerItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieManager;
import java.util.ArrayList;

public class ExamActivity extends AppCompatActivity {
    private SQLiteHandler db;
    private SessionManager session;
    private ProgressDialog pDialog;
    protected RecyclerView mRecyclerView;
    public TextView tv_student_id;
    public TextView tv_student_name;
    public TextView tv_student_class;
   public String st_id;
   public String cl_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        tv_student_id = (TextView) findViewById(R.id.student_id);
        tv_student_name = (TextView) findViewById(R.id.student_name);
        tv_student_class = (TextView) findViewById(R.id.student_class);
        mRecyclerView = (RecyclerView) findViewById(R.id.cardlist_student);
        CookieManager cookieManage = new CookieManager();
        cookieManage.getCookieStore();
//        CookieHandler.setDefault(cookieManage);
        // session manager
        session = new SessionManager(getApplicationContext());
        session.setLogin(true);

//        Bundle extras = getIntent().getExtras();
        Intent intent = getIntent();
        if (intent != null) {
            st_id = intent.getStringExtra("STUDENT_ID");
            cl_id = intent.getStringExtra("CLASS_ID");
            Toast.makeText(this, st_id+" and "+ cl_id, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "data not received", Toast.LENGTH_SHORT).show();
        }
        getExamJson();
    }

    private void getExamJson() {
        String tag_string_req = "req_data";

        pDialog.setMessage("Fetching data ...");
        showDialog();

        JsonArrayRequest req = new JsonArrayRequest(Request.Method.GET,
                AppConfig.URL_GET_EXAM, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                try {
                    hideDialog();

//                        JSONObject jObj = new JSONObject(response);
                    Log.i("tagexamresponse", "[" + response.toString() + "]");
//                    JSONObject jObj = new JSONObject(response);
////
////                    JSONObject jsonObject = new JSONObject("student");
//                    JSONArray result = jObj.getJSONArray("student");
                    ArrayList<Exam> listdata = new ArrayList<>();
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject jo = response.getJSONObject(i);
                        Exam exams = new Exam();
                        exams.setExamName(jo.getString(AppConfig.TAG_EXAM_NAME));
                        exams.setExamId(jo.getString(AppConfig.TAG_EXAM_ID));


                        listdata.add(exams);
                        showExamList(listdata);
                    }
                } catch (JSONException e) {
                    hideDialog();
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideDialog();
//                        Log.i("error", error.getMessage());
                    }
                }) {
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req, tag_string_req);
    }

    private void showExamList(final ArrayList<Exam> listdata) {
        mRecyclerView = (RecyclerView) findViewById(R.id.cardlist_exam);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(new ExamCardAdapter(listdata));

        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                          String ex_id= listdata.get(position).getExamId();
                        String class_id= st_id;
                        Intent intent = new Intent(ExamActivity.this, MarksheetActivity.class);
                        intent.putExtra("EXAM_ID", ex_id);
                        intent.putExtra("STUDENT_ID", st_id);
                        startActivity(intent);
                    }
                })
        );

    }
    @Override
    public void onBackPressed() {

            super.onBackPressed();

    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }



}
