package com.schoolapp.rashim12000.schoolims;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.schoolapp.rashim12000.schoolims.Adapter.RecyclerItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieManager;
import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class StudentActivityFragment extends Fragment {
    private SQLiteHandler db;
    private SessionManager session;
    private ProgressDialog pDialog;
    protected RecyclerView mRecyclerView;
    public TextView tv_student_id;
    public TextView tv_student_name;
    public TextView tv_student_class;
    private SharedPreferences sharedPreferences;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = getActivity().getSharedPreferences("studentdetail",0);
    }

    public StudentActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_student, container, false);
        // Progress dialog
        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);

        tv_student_id = (TextView) rootView.findViewById(R.id.student_id);
        tv_student_name = (TextView) rootView.findViewById(R.id.student_name);
        tv_student_class = (TextView) rootView.findViewById(R.id.student_class);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.cardlist_student);
        CookieManager cookieManage = new CookieManager();
        cookieManage.getCookieStore();

//        CookieHandler.setDefault(cookieManage);
        // session manager
        session = new SessionManager(getActivity().getApplicationContext());
        session.setLogin(true);

        getChildJson();

        return rootView;
    }

    public void getChildJson() {

        String tag_string_req = "req_data";

        pDialog.setMessage("Fetching data ...");
        showDialog();

        JsonArrayRequest req = new JsonArrayRequest(Request.Method.GET,
                AppConfig.URL_GET_S_INFO, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                try {
                    hideDialog();

//                        JSONObject jObj = new JSONObject(response);
                    Log.i("tagconvertresponse", "[" + response.toString() + "]");
//                    JSONObject jObj = new JSONObject(response);
////
////                    JSONObject jsonObject = new JSONObject("student");
//                    JSONArray result = jObj.getJSONArray("student");
                    ArrayList<Students> listdata = new ArrayList<>();
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject jo = response.getJSONObject(i);
                        Students students = new Students();
                        students.setStudentId(jo.getString(AppConfig.TAG_SID_S));
                        students.setStudentName(jo.getString(AppConfig.TAG_SN_S));
                        students.setStudentClass(jo.getString(AppConfig.TAG_CID_S));

                        listdata.add(students);
                        showStudent(listdata);
                    }
                } catch (JSONException e) {
                    hideDialog();
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideDialog();
//                        Log.i("error", error.getMessage());
                    }
                }) {
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req, tag_string_req);


    }

    private void showStudent(final ArrayList<Students> listdata) {
//        Log.i(TAG, "we are here");

//        List<Students> studentsList = new ArrayList<>();
        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getActivity(), resId);
        mRecyclerView.setLayoutAnimation(animation);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(new StudentsCardAdapter(listdata));

        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        SharedPreferences.Editor editor = sharedPreferences.edit();
//                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + n));
//                        startActivity(intent);

//                        Toast.makeText(getActivity(), listdata.get(position).getStudentId(), Toast.LENGTH_SHORT).show();
                        String st_id= listdata.get(position).getStudentId();
                        String class_id= listdata.get(position).getStudentClass();
                        String stdname = listdata.get(position).getStudentName();
                        Intent intent = new Intent(getActivity(), StudentOptionActivity.class);
                        intent.putExtra("STUDENT_ID", st_id);
                        intent.putExtra("CLASS_ID", class_id);

                        editor.putString("STUDENT_ID",st_id);
                        editor.putString("CLASS_ID",class_id);
                        editor.putString("STUDENT_NAME",stdname);
                        editor.apply();
                        startActivity(intent);
                        new ActivityAnimator().fadeAnimation(getActivity());
                    }
                })
        );

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


}
