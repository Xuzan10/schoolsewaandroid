package com.schoolapp.rashim12000.schoolims;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Rashim12000 on 10/18/2017.
 */

public class StudentsCardAdapter extends RecyclerView.Adapter<StudentsCardAdapter.ContactViewHolder> {

    List<Students> items;
    int itemLayout;
    Context context;
    OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);

        public void onItemClickExpand(View view, int position);

    }

    public StudentsCardAdapter(List<Students> items) {
        this.items = items;
        onItemClickListener = onItemClickListener;
//        this.itemLayout = itemLayout;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_student, parent, false);
        return new ContactViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, final int position) {
        holder.setIsRecyclable(false);
        Students item = items.get(position);
        holder.tv_student_id.setText(item.getStudentId());
        holder.tv_student_class.setText("Class : "+item.getStudentClass());
        holder.tv_student_name.setText(item.getStudentName());


//        holder.container.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onItemClickListener.onItemClick(v, position);
//
////                Students students = new Students()
//
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public static class ContactViewHolder extends RecyclerView.ViewHolder {

        public static TextView tv_student_id;
        public static TextView tv_student_class;
        public static TextView tv_student_name;
        public View container;

        public ContactViewHolder(View v) {
            super(v);
            container = v;
            tv_student_id = (TextView) v.findViewById(R.id.student_id);
            tv_student_name = (TextView) v.findViewById(R.id.student_name);
            tv_student_class = (TextView) v.findViewById(R.id.student_class);

        }
    }


}
