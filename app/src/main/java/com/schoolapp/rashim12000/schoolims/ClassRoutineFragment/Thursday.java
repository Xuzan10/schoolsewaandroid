package com.schoolapp.rashim12000.schoolims.ClassRoutineFragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.schoolapp.rashim12000.schoolims.Adapter.ClassRoutineCardAdapter;
import com.schoolapp.rashim12000.schoolims.AppConfig;
import com.schoolapp.rashim12000.schoolims.AppController;
import com.schoolapp.rashim12000.schoolims.ClassRoutine;
import com.schoolapp.rashim12000.schoolims.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieManager;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Rashim12000 on 11/5/2017.
 */

public class Thursday extends Fragment {
    private ArrayList<ClassRoutine> listdata;


    public Thursday() {

    }

    private ProgressDialog pDialog;

    protected RecyclerView mRecyclerView;
    //private String testurl = "http://192.168.100.3/test/test.php";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Progress dialog
        Log.i("day5","thursday");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_class_routine, container, false);
        CookieManager cookieManage = new CookieManager();
        cookieManage.getCookieStore();
        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);
        pDialog.setMessage("Fetching data ...");

        mRecyclerView = (RecyclerView) v.findViewById(R.id.cardlist_class_routine);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        if(listdata==null) {
            getClassRoutineJson();
        }else {
            showNotice(listdata);
        }
        return v;
    }

    private void getClassRoutineJson() {
        showDialog();

        String tag_string_req = "req_data";


        String routine_url = AppConfig.URL_GET_CLASS_ROUTINE + "?class_id=" + 6 + "&day=" + 4;

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,
                routine_url/*testurl*/, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                hideDialog();

                try {

//                        JSONObject jObj = new JSONObject(response);
                    Log.i("tagconvertresponse", "[" + response.toString() + "]");
//                    JSONObject jObj = new JSONObject(response);
////
////                    JSONObject jsonObject = new JSONObject("student");
//                   JSONArray result = jObj.getJSONArray("student");
                    JSONObject subjson = response.getJSONObject("4");

                    listdata = new ArrayList<>();
                    Iterator iterator = subjson.keys();
                    List<String> keylist = new ArrayList<String>();
                    while (iterator.hasNext()) {
                        String key = (String) iterator.next();
                        keylist.add(key);

                    }
                    for (int j = 0; j < keylist.size(); j++) {
                        ClassRoutine routine = new ClassRoutine();
                        JSONObject period = subjson.getJSONObject(keylist.get(j));

                        Log.d("periodvalue", period.toString());
                        routine.setPeriodName(keylist.get(j));
                        routine.setPeriodSubject(period.getString(/*AppConfig.TAG_NOTICE_BODY*/ AppConfig.TAG_CLASS_ROUTINE_SUBJECT_NAME));
                        routine.setPeriodTeacher(period.getString(/*AppConfig.TAG_NOTICE_BODY*/ AppConfig.TAG_CLASS_ROUTINE_SUBJECT_TEACHER));
                        routine.setPeriodStartTime(period.getString(/*AppConfig.TAG_NOTICE_BODY*/ AppConfig.TAG_CLASS_ROUTINE_START_TIME));
                        routine.setPeriodEndTime(period.getString(/*AppConfig.TAG_NOTICE_BODY*/ AppConfig.TAG_CLASS_ROUTINE_END_TIME));
                        //  routine.setPeriodTime(jo.getString(/*AppConfig.TAG_NOTICE_BODY*/));

                        listdata.add(routine);
                    }

                    Log.i("list", listdata.get(0).getPeriodSubject());

                    Log.i("list", listdata.get(1).getPeriodSubject());
                    hideDialog();
                    showNotice(listdata);


//                        routine.setPeriodName(jo.getString(/*AppConfig.TAG_NOTICE_TITLE*/));

//                        notice.setStudentClass(jo.getString(AppConfig.TAG_CID_S));


                } catch (JSONException e) {
                    hideDialog();

                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideDialog();
                      //  Log.i("error", error.getMessage());
                    }
                }) {
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req, tag_string_req);


    }

    private void showNotice(ArrayList<ClassRoutine> listdata) {
        hideDialog();

        mRecyclerView.setAdapter(new ClassRoutineCardAdapter(listdata, R.layout.cardview_class_routine));

//        mRecyclerView.addOnItemTouchListener(
//                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(View view, int position) {
//                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + n));
//                        startActivity(intent);
//
//                    }
//                })
//        );
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }



}
