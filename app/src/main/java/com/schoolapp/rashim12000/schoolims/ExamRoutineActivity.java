package com.schoolapp.rashim12000.schoolims;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.schoolapp.rashim12000.schoolims.Adapter.ExamRoutineAdapter;
import com.schoolapp.rashim12000.schoolims.Adapter.ExamSubjectAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ExamRoutineActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView,mRecyclerView2;
    private SharedPreferences sharedPreferences;
    String class_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_routine);
        sharedPreferences = getSharedPreferences("studentdetail",0);
        class_id = sharedPreferences.getString("CLASS_ID","");
        mRecyclerView = (RecyclerView) findViewById(R.id.Recycler);
        mRecyclerView2 = (RecyclerView) findViewById(R.id.Recycler2);


        getroutinejson();

    }

    private void getroutinejson() {
        String req_tag = "req_exam_routine";

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, AppConfig.URL_GET_EXAM_ROUTINE, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {


                    try {

                        Log.i("response",response.toString());

                        ArrayList<ExamRoutine> list = new ArrayList<>();
                        ArrayList<Subjects> list2 = new ArrayList<>();
                        for (int i = 0;i<response.length();i++) {
                            ExamRoutine routine = new ExamRoutine();
                            JSONObject object = response.getJSONObject(i);
                            routine.setClass_id(object.optString("class_id"));
                            routine.setExam_number(object.optString("exam_number"));
                            routine.setClass_name(object.optString("class_name"));
                            routine.setExam_name(object.optString("exam_name"));
                            JSONArray array = object.getJSONArray("subjects");
                            list.add(routine);
                            showlist(list);


                            for (int j=0;j<array.length();j++){
                                JSONObject object2 = array.getJSONObject(j);
                                Subjects sub = new Subjects();
                                Log.i("object",object2.toString());
                                sub.setSn(String.valueOf(j+1));
                                sub.setSubject_name(object2.optString("subject_name"));
                                sub.setDay(object2.optString("day"));
                                sub.setExam_date(object2.optString("exam_date"));
                                list2.add(sub);
                                showsublist(list2);

                            }




                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        AppController.getInstance().addToRequestQueue(request,req_tag);
    }

    private void showsublist(ArrayList<Subjects> list2) {

        mRecyclerView2.setHasFixedSize(true);
        mRecyclerView2.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView2.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView2.setAdapter(new ExamSubjectAdapter(list2,this));

    }


    private void showlist(ArrayList<ExamRoutine> list) {

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(new ExamRoutineAdapter(list,this));

    }

}
