package com.schoolapp.rashim12000.schoolims;

/**
 * Created by DEll on 11/27/2017.
 */

public class Subjects {


    private String sn,subject_name, day, exam_date;

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getExam_date() {
        return exam_date;
    }

    public void setExam_date(String exam_date) {
        this.exam_date = exam_date;
    }

    public Subjects() {

    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Subjects(String sn, String subject_name, String day, String exam_date) {

        this.sn = sn;
        this.subject_name = subject_name;
        this.day = day;
        this.exam_date = exam_date;
    }
}
