package com.schoolapp.rashim12000.schoolims.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.schoolapp.rashim12000.schoolims.Notice;
import com.schoolapp.rashim12000.schoolims.R;

import java.util.List;

/**
 * Created by Rashim12000 on 10/31/2017.
 */

public class NoticeCardAdapter extends RecyclerView.Adapter<NoticeCardAdapter.NoticeViewHolder> {

    List<Notice> items;
    private int itemLayout;

    public NoticeCardAdapter(List<Notice> items, int itemLayout){
        this.items = items;
        this.itemLayout = itemLayout;
    }
    @Override
    public NoticeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new NoticeViewHolder(v);
    }

    @Override
    public void onBindViewHolder(NoticeViewHolder holder, int position) {
        Notice item = items.get(position);
        holder.tv_notice_title.setText(item.getNoticeTitle());
        holder.tv_notice_body.setText(item.getNoticeBody());
//        holder.tv_notice_image.setImageResource(item.getNoticeImage());

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class NoticeViewHolder extends RecyclerView.ViewHolder {

        public static TextView tv_notice_title;
        protected static TextView tv_notice_body;
//        protected static ImageView tv_notice_image;

        public NoticeViewHolder(View v) {
            super(v);

            tv_notice_title = (TextView) v.findViewById(R.id.notice_title);
            tv_notice_body = (TextView) v.findViewById(R.id.notice_body);
//            tv_notice_image=(ImageView) v.findViewById(R.id.notice_image_view);
        }
    }
}