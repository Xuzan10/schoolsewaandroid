package com.schoolapp.rashim12000.schoolims;


public class AppConfig {
    /*// Server user login url
    public static String URL_LOGIN = "http://10.0.2.2/driver/login.php";

    // Server user register url
    public static String URL_REGISTER = "http://10.0.2.2/driver/register.php";*/

    //    public static String URL_LOGIN = "http://192.168.0.103/school/login.php";
    public static String MAIN = "http://schoolsewa.herokuapp.com/public/";
    //public static String MAIN = "http://rcmcms.herokuapp.com/public/";

    public static String MAIN_URL = "http://192.168.100.10:8000/";
    public static String URL = "http://127.0.0.1:8000";
    //public static String URL_GET_TOKEN = "http://192.168.0.103:8000/api/get-token";
    // public static String URL_LOGIN = "http://192.168.0.103:8000/api/register-device";
    public static String URL_GET_TOKEN = MAIN+"api/get-token";
    public static String URL_LOGIN = MAIN+"api/register-device";


    public static final String URL_GET_S_INFO = MAIN + "api/guardian/student-info";//working without laravel
    public static final String URL_GET_FEEDBACK = MAIN + "api/guardian/get-feedback";
    public static final String URL_SEND_FEEDBACK = MAIN + "api/guardian/send-feedback";
    public static final String URL_GET_ALL_NOTICE = MAIN + "api/guardian/get-notice-all";
    public static final String URL_GET_STUDENT_NOTICE = MAIN + "api/guardian/get-notice-student";
    public static final String URL_GET_EXAM = MAIN + "api/guardian/get-exam";
    public static final String URL_GET_MARKSHEET = MAIN + "api/guardian/get-marksheet";
    public static final String URL_GET_CLASS_ROUTINE = MAIN + "api/guardian/get-routine";
    public static final String URL_GET_DRIVER = MAIN + "api/guardian/get-driver";
    public static final String URL_GET_EXAM_ROUTINE = MAIN + "api/guardian/get-exam-routine";



    public static final String URL_GET_G_INFO = "http://192.168.0.103/school/guardian/getGuardianInfo.php";
    public static final String URL_GET_T_INFO = "http://192.168.0.103/school/teacher/getTeacherInfo.php";
    public static final String URL_GET_MS_INFO = "http://192.168.0.103/school/marksheet/getMarksheetInfo.php";
    public static final String URL_GET_SUB_INFO = "http://192.168.0.103/school/subject/getSubjectInfo.php";
    public static final String URL_GET_R_INFO = "http://192.168.0.103/school/routine/getRoutineInfo.php";

    //JSON Tags

    public static final String TAG_JSON_ARRAY_S = "response";
    public static final String TAG_SID_S = "student_id";
    public static final String TAG_UID_S = "user_id";
    public static final String TAG_CID_S = "class_id";
    public static final String TAG_GID_S = "guardian_id";
    public static final String TAG_SN_S = "student_name";
    public static final String TAG_A_S = "address";
    public static final String TAG_C_S = "contact";
    public static final String TAG_B_S = "birthdate";
    public static final String TAG_G_S = "gender";


    public static final String TAG_FEEDBACK = "feedback";


    public static final String TAG_JSON_ARRAY_G = "result";
    public static final String TAG_GID_G = "guardian_id";
    public static final String TAG_UID_G = "user_id";
    public static final String TAG_GN_G = "guardian_name";
    public static final String TAG_A_G = "address";
    public static final String TAG_M_G = "mobile_no";
    public static final String TAG_H_G = "home_no";


    public static final String TAG_JSON_ARRAY_T = "result";
    public static final String TAG_TID_T = "teacher_id";
    public static final String TAG_UID_T = "user_id";
    public static final String TAG_TN_T = "teacher_name";
    public static final String TAG_A_T = "address";
    public static final String TAG_M_T = "mobile_no";
    public static final String TAG_Q_T = "qualification";

    public static final String TAG_JSON_ARRAY_MS = "result";
    public static final String TAG_MS_ID = "marksheet_id";
    public static final String TAG_MS_SID = "student_id";
    public static final String TAG_MS_CID = "class_id";
    public static final String TAG_MS_EID = "exam_id";
    public static final String TAG_MS_SuID = "subject_id";
    public static final String TAG_MS_PM = "pr_marks";
    public static final String TAG_MS_OM = "obt_marks";

    public static final String TAG_JSON_ARRAY_SUB = "result";
    public static final String TAG_SUB_ID = "subject_id";
    public static final String TAG_SUB_N = "subject_name";
    public static final String TAG_SUB_FM = "full_marks";
    public static final String TAG_SUB_PM = "pass_marks";

    public static final String TAG_JSON_ARRAY_R = "result";
    public static final String TAG_TCSId_R = "teacher_class_subject_id";
    public static final String TAG_TId_R = "teacher_id";
    public static final String TAG_TC_R = "teacher_class";
    public static final String TAG_TS_R = "teacher_subject";
    public static final String TAG_P_R = "period";
    public static final String TAG_D_R = "day";
    public static final String TAG_ST_R = "start_time";
    public static final String TAG_ET_R = "end_time";


    public static final String TAG_NOTICE_TITLE = "notice_title";
    public static final String TAG_NOTICE_BODY = "notice_subject";
    public static final String TAG_NOTICE_NAME = "student_name";
    public static final String TAG_NOTICE_DATE = "notice_date";


    public static final String TAG_EXAM_ID = "exam_id";
    public static final String TAG_EXAM_NAME = "exam_name";


    public static final String TAG_MARKSHEET_STUDENT = "student_name";
    public static final String TAG_MARKSHEET_EXAM = "exam_name";
    public static final String TAG_MARKSHEET_CLASS = "class_name";
    public static final String TAG_MARKSHEET_HAS_FILE = "has_file";
    public static final String TAG_MARKSHEET_FILE_NAME = "marksheet_file";
    public static final String TAG_MARKSHEET_SUBJECTS = "subjects";
    public static final String TAG_MARKSHEET_OBT_MARKS = "obt_marks";


    public static final String TAG_CLASS_ROUTINE_SUBJECT_NAME = "subject_name";
    public static final String TAG_CLASS_ROUTINE_SUBJECT_TEACHER = "teacher_name";
    public static final String TAG_CLASS_ROUTINE_START_TIME = "start_time";
    public static final String TAG_CLASS_ROUTINE_END_TIME = "end_time";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";
    public static final String SHARED_PREF = "ah_firebase";


}