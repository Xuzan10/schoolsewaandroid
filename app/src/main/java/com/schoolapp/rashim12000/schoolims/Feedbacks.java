package com.schoolapp.rashim12000.schoolims;

/**
 * Created by Rashim12000 on 10/25/2017.
 */

class Feedbacks {
    String feedbacks;

    public  Feedbacks(){}
    public Feedbacks(String feedbacks) {
        this.feedbacks = feedbacks;
    }

    public String getFeedbacks() {
        return feedbacks;
    }

    public void setFeedbacks(String feedbacks) {
        this.feedbacks = feedbacks;
    }
}
