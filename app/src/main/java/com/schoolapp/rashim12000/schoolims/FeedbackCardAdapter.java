package com.schoolapp.rashim12000.schoolims;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Rashim12000 on 10/18/2017.
 */

public class FeedbackCardAdapter extends RecyclerView.Adapter<FeedbackCardAdapter.ContactViewHolder> {

    List<Feedbacks> items;
    int itemLayout;
    Context context;
    OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);

        public void onItemClickExpand(View view, int position);

    }

    public FeedbackCardAdapter(List<Feedbacks> items, OnItemClickListener onItemClickListener) {
        this.items = items;
        onItemClickListener = onItemClickListener;
//        this.itemLayout = itemLayout;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_feedback, parent, false);
        return new ContactViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, final int position) {
        holder.setIsRecyclable(false);
        Feedbacks item = items.get(position);
        holder.tv_feedback.setText(item.getFeedbacks());



        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(v, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public static class ContactViewHolder extends RecyclerView.ViewHolder {

        public static TextView tv_feedback;

        public View container;

        public ContactViewHolder(View v) {
            super(v);
            container = v;
            tv_feedback = (TextView) v.findViewById(R.id.feedbacks);


        }
    }


}
