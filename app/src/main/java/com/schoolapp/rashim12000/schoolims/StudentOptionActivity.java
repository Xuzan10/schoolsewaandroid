package com.schoolapp.rashim12000.schoolims;

import android.*;
import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.firebase.messaging.FirebaseMessaging;
import com.loopj.android.http.PersistentCookieStore;
import com.tapadoo.alerter.Alerter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieManager;
import java.util.ArrayList;

public class StudentOptionActivity extends AppCompatActivity {
    private SessionManager session;
    private ProgressDialog pDialog;

    Button btn_marksheet;
    Button btn_notice;
    Button btn_class_routine, view_student_driver, exam_routine, attendace;
    SharedPreferences sharedPreferences;

    public String st_id;
    public String cl_id;
    private String stdname;
    private TextView stdtext;

    private CardView marksheetcard, noticecard,classs_routine_card, driver_card,exam_routine_card,attendacecard;
     BroadcastReceiver mRegistrationBroadcastReceiver;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_option);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        sharedPreferences = getSharedPreferences("studentdetail",0);
        st_id = sharedPreferences.getString("STUDENT_ID","");
        stdname = sharedPreferences.getString("STUDENT_NAME","");
        stdtext = (TextView) findViewById(R.id.stdname);


        CookieManager cookieManage = new CookieManager();
        cookieManage.getCookieStore();

        stdtext.setText(stdname);

//        CookieHandler.setDefault(cookieManage);
        // session manager
        session = new SessionManager(getApplicationContext());
        session.setLogin(true);

//        Bundle extras = getIntent().getExtras();
        Intent intent = getIntent();
        if (intent != null) {
            cl_id = intent.getStringExtra("CLASS_ID");
           // Toast.makeText(this, st_id + " and " + cl_id, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "data not received", Toast.LENGTH_SHORT).show();
        }

        attendace = (Button) findViewById(R.id.attendace);
        attendacecard = (CardView) findViewById(R.id.attendace_card);

        btn_marksheet = (Button) findViewById(R.id.marksheet);
        btn_notice = (Button) findViewById(R.id.notice);
        btn_class_routine = (Button) findViewById(R.id.class_routine);
        view_student_driver = (Button) findViewById(R.id.view_student_driver);
        exam_routine = (Button) findViewById(R.id.exam_routine);

        marksheetcard = (CardView) findViewById(R.id.marksheetcard);
        noticecard = (CardView) findViewById(R.id.noticecard);
        classs_routine_card  = (CardView) findViewById(R.id.class_routine_card);
        driver_card = (CardView) findViewById(R.id.driver_card);
        exam_routine_card = (CardView) findViewById(R.id.exam_routine_card);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(AppConfig.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(AppConfig.TOPIC_GLOBAL);


                } else if (intent.getAction().equals(AppConfig.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");
                    Alerter.create(StudentOptionActivity.this)
                            .setTitle("Notice!!!")
                            .enableVibration(true)
                            .setText(message)
                            .setBackgroundColorRes(R.color.green)
                            .enableSwipeToDismiss()
                            .enableInfiniteDuration(true)
                            .show();
                    // Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();


                }
            }
        };
        btn_marksheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(StudentOptionActivity.this, MarksheetActivityFragment.class);
                intent1.putExtra("STUDENT_ID", st_id);
                intent1.putExtra("CLASS_ID", cl_id);
                startActivity(intent1);

            }
        });

        attendacecard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(StudentOptionActivity.this,StudentAttendanceActivity.class));

            }
        });

        btn_notice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        btn_class_routine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(StudentOptionActivity.this, ScrollableClassRoutineTabsActivity.class);
                intent1.putExtra("STUDENT_ID", st_id);
                intent1.putExtra("CLASS_ID", cl_id);
                startActivity(intent1);
            }
        });

        view_student_driver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkLocationPermission();
                final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    buildAlertMessageNoGps();
                } else {
                    Intent i = new Intent(StudentOptionActivity.this, DisplayActivity.class);
                    i.putExtra("STUDENT_ID", st_id);
                    Log.i("student_id", st_id);
                    i.putExtra("CLASS_ID", cl_id);
                    startActivity(i);
                }



            }
        });

        exam_routine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(StudentOptionActivity.this, ExamRoutineActivity.class));
            }
        });

        marksheetcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(StudentOptionActivity.this, MarksheetActivityFragment.class);
                intent1.putExtra("STUDENT_ID", st_id);
                intent1.putExtra("CLASS_ID", cl_id);
                startActivity(intent1);

            }
        });

        noticecard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        classs_routine_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(StudentOptionActivity.this, ScrollableClassRoutineTabsActivity.class);
                intent1.putExtra("STUDENT_ID", st_id);
                intent1.putExtra("CLASS_ID", cl_id);
                startActivity(intent1);
            }
        });

        driver_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkLocationPermission();
                final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    buildAlertMessageNoGps();
                } else {
                    Intent i = new Intent(StudentOptionActivity.this, DisplayActivity.class);
                    i.putExtra("STUDENT_ID", st_id);
                    Log.i("student_id", st_id);
                    i.putExtra("CLASS_ID", cl_id);
                    startActivity(i);
                }



            }
        });

        exam_routine_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(StudentOptionActivity.this, ExamRoutineActivity.class));
            }
        });
    }



    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(StudentOptionActivity.this,
                                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }

    }

    public void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                });

        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
           finish();
        }
        return true;


    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
        new ActivityAnimator().fadeAnimation(StudentOptionActivity.this);

        finish();
        super.onBackPressed();
    }
}
