package com.schoolapp.rashim12000.schoolims;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieManager;
import java.util.ArrayList;

public class MarksheetDisplayActivity extends AppCompatActivity {

    private TextView schoolname, schoollocation, studentname, studentclass, studentroll, resultdate, examname, totalmarks;
    private String st_id;
    private String ex_id;
    private SharedPreferences sharedPreferences;
    private SessionManager session;
    private ProgressDialog pDialog;


    private String[] subject;
    private String[] DataSub;
    private String[] DataObt;
    TableLayout tableview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marksheet_display);
        CookieManager cookieManage = new CookieManager();
        cookieManage.getCookieStore();
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        session = new SessionManager(getApplicationContext());
        session.setLogin(true);
        schoolname = (TextView) findViewById(R.id.school_name);
        schoollocation = (TextView) findViewById(R.id.school_location);
        studentname = (TextView) findViewById(R.id.student_name);
        studentclass = (TextView) findViewById(R.id.student_class);
        studentroll = (TextView) findViewById(R.id.student_roll);
        resultdate = (TextView) findViewById(R.id.result_date);
        examname = (TextView) findViewById(R.id.exam_name);
        totalmarks = (TextView) findViewById(R.id.totalmarks);

        tableview = (TableLayout) findViewById(R.id.tableViewMarksheet);


        sharedPreferences = getSharedPreferences("studentdetail", 0);
        st_id = sharedPreferences.getString("STUDENT_ID", "");

        ex_id = getIntent().getStringExtra("EXAM_ID");


        getMarksJson();


    }

    private void getMarksJson() {
        pDialog.setMessage("Fetching data ...");
        showDialog();
        String tag_string_req = "req_data";
        String url = AppConfig.URL_GET_MARKSHEET + "?exam_id=" + ex_id + "&student_id=" + st_id;
        Log.i("marksheet url", url);
        JsonArrayRequest req = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                hideDialog();
                ArrayList<Marksheet> listdata = new ArrayList<>();
                subject = new String[10];

                try {

                    for (int i = 0; i < response.length(); i++) {
                        JSONObject jo = response.getJSONObject(i);
                        Marksheet marksheet = new Marksheet();
                        marksheet.setExamName(jo.getString(AppConfig.TAG_MARKSHEET_EXAM));
                        SpannableString content = new SpannableString(jo.optString(AppConfig.TAG_MARKSHEET_EXAM));
                        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                        examname.setText(content);

                        marksheet.setStudnetName(jo.getString(AppConfig.TAG_MARKSHEET_STUDENT));

                        studentname.setText(jo.optString(AppConfig.TAG_MARKSHEET_STUDENT));

                        marksheet.setClassName(jo.getString(AppConfig.TAG_MARKSHEET_CLASS));
                        studentclass.setText(jo.getString(AppConfig.TAG_MARKSHEET_CLASS));

                        marksheet.setHasFile(Boolean.parseBoolean(jo.getString(AppConfig.TAG_MARKSHEET_HAS_FILE)));
                        marksheet.setSn(String.valueOf(i + 1));


                        if ((Boolean.parseBoolean(jo.getString(AppConfig.TAG_MARKSHEET_HAS_FILE)))) {

                            Toast.makeText(MarksheetDisplayActivity.this, "View Image", Toast.LENGTH_SHORT).show();
                            marksheet.setFileName((jo.getString(AppConfig.TAG_MARKSHEET_FILE_NAME)));
                            listdata.add(marksheet);

//                            showMarksheetList(listdata);

                        } else {


                            String sub = (jo.getString(AppConfig.TAG_MARKSHEET_SUBJECTS));
                            String obt = (jo.getString(AppConfig.TAG_MARKSHEET_OBT_MARKS));

                            Log.i("sub sub", sub);
                            subject[0] = sub;
                            subject[1] = obt;
                            subject[0] = subject[0].replaceAll("\\[", "").replaceAll("\\]", "");
                            subject[0] = subject[0].replace("\"", "");
                            subject[1] = subject[1].replaceAll("\\[", "").replaceAll("\\]", "");
                            subject[1] = subject[1].replace("\"", "");
// subject[0]=subject[0].replaceAll(","," ");
                            DataSub = subject[0].split(",");

                            DataObt = subject[1].split(",");
//


                            marksheet.setSubjects(DataSub);
                            marksheet.setObtMarks(DataObt);

                            listdata.add(marksheet);

                            showMarksheetList(listdata);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    hideDialog();
                    Toast.makeText(MarksheetDisplayActivity.this, "Marksheet not added", Toast.LENGTH_LONG).show();

                }


            }


        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideDialog();

            }
        });
        AppController.getInstance().

                addToRequestQueue(req, tag_string_req);
    }

    private void showMarksheetList(ArrayList<Marksheet> listdata) {

        Log.i("data length", "length" + DataSub.length);
        for (int k = 0; k < DataSub.length; k++) {


            Log.i("data data", "datasub =" + DataSub[k]);
            Log.i("data data", "datasub =" + DataObt[k]);

            TableRow tbrow = new TableRow(this);
            TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(40, 20, 200, 20);
            tbrow.setLayoutParams(layoutParams);


            TextView sn = new TextView(this);
            sn.setText(String.valueOf(k + 1));
//            t1v.setTextColor(Color.WHITE);
            sn.setGravity(Gravity.CENTER);
            tbrow.addView(sn, layoutParams);


            TextView t1v = new TextView(this);
            t1v.setText(DataSub[k]);
//            t1v.setTextColor(Color.WHITE);
            t1v.setGravity(Gravity.CENTER);
            tbrow.addView(t1v, layoutParams);
//            TextView t2v = new TextView(this);
//            t2v.setText(subFm);
//            t2v.setTextColor(Color.WHITE);
//            t2v.setGravity(Gravity.CENTER);
//            tbrow.addView(t2v);
//            TextView t3v = new TextView(this);
//            t3v.setText(subPm);
//            t3v.setTextColor(Color.WHITE);
//            t3v.setGravity(Gravity.CENTER);
//            tbrow.addView(t3v);
            TextView t4v = new TextView(this);
            t4v.setText(DataObt[k]);
//            t4v.setTextColor(Color.WHITE);
            t4v.setGravity(Gravity.CENTER);
            tbrow.addView(t4v, layoutParams);
            tableview.addView(tbrow);

        }


        double sum = 0.0;
        for (String element : DataObt) {
            double num = Double.parseDouble(element);
            sum += num;
        }
        totalmarks.setText(String.valueOf(sum));

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), StudentOptionActivity.class));
        finish();
    }
}
