package com.schoolapp.rashim12000.schoolims.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.schoolapp.rashim12000.schoolims.Exam;
import com.schoolapp.rashim12000.schoolims.FeedbackCardAdapter;
import com.schoolapp.rashim12000.schoolims.Notice;
import com.schoolapp.rashim12000.schoolims.R;

import java.util.List;

/**
 * Created by Rashim12000 on 10/31/2017.
 */

public class ExamCardAdapter extends RecyclerView.Adapter<ExamCardAdapter.ExamViewHolder> {

    List<Exam> items;
    private int itemLayout;

    ExamCardAdapter.OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);

        public void onItemClickExpand(View view, int position);

    }
    public ExamCardAdapter(List<Exam> items){
        this.items = items;
//        this.itemLayout = itemLayout;
    }
    @Override
    public ExamViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_exam, parent, false);
        return new ExamViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ExamViewHolder holder, final int position) {
        Exam item = items.get(position);
        holder.tv_exam_title.setText(item.getExamName());
        holder.tv_exam_id.setText(item.getExamId());

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ExamViewHolder extends RecyclerView.ViewHolder {

        public static TextView tv_exam_title;
        protected static TextView tv_exam_id;
//        protected static ImageView tv_notice_image;
public View container;
        public ExamViewHolder(View v) {
            super(v);
            container=v;
            tv_exam_title = (TextView) v.findViewById(R.id.exam_name);
            tv_exam_id = (TextView) v.findViewById(R.id.exam_id);

        }
    }
}