package com.schoolapp.rashim12000.schoolims;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.firebase.messaging.FirebaseMessaging;
import com.rom4ek.arcnavigationview.ArcNavigationView;
import com.tapadoo.alerter.Alerter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = HomeActivity.class.getSimpleName();
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    private String id="";

    public static String role_id;
    public static String user_id;
    String guardian_id;
    String guardian_name;
    public static TextView tv_user_name;
    public static TextView tv_user_email;

    public TextView tv_student_id;
    public TextView tv_student_name;
    public TextView tv_student_class;
    private String JSON_STRING;
    private String CHILD_JSON_STRING;

    private String[] studentName;
    private String[] studentId;
    private String[] classId;

    private SQLiteHandler db;
    private SessionManager session;
    private ProgressDialog pDialog;
    protected RecyclerView mRecyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        id = getIntent().getStringExtra("id");
        if(id!=null) {
            Log.i("test", id);
        }else
        {
            Log.i("test","test null");
        }
        Fragment fragment = null;
        Class fragmentClass = null;
        if (id!=null && id.equals("notice")) {     // checking notice putextra from notification
            fragmentClass = ScrollableNoticeTabsActivity.class;
        }else {
            fragmentClass = StudentActivityFragment.class;
        }
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
//
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        CookieManager cookieManage = new CookieManager();
        cookieManage.getCookieStore();


//        CookieHandler.setDefault(cookieManage);
        // session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }
        FirebaseMessaging.getInstance().subscribeToTopic("global");
        displayFirebaseRegId();
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(AppConfig.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(AppConfig.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(AppConfig.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");
                    Alerter.create(HomeActivity.this)
                            .setTitle("Notice!!!")
                            .setText(message)
                            .setBackgroundColorRes(R.color.green)
                            .enableSwipeToDismiss()
                            .enableInfiniteDuration(true)
                            .setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                                    i.putExtra("id","notice");
                                    startActivity(i);
                                }
                            })
                            .show();
                    // Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();


                }
            }
        };


        // Fetching user details from sqlite
        HashMap<String, String> user = db.getUserDetails();

        user_id = user.get("user_id");
        role_id = user.get("role_id");
        String email = user.get("email");
//        Integer i = Integer.parseInt(role_id);

        guardian_id = user.get("guardian_id");
        guardian_name = user.get("guardian_name");

        ArcNavigationView navigationView = (ArcNavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        tv_user_name = (TextView) header.findViewById(R.id.userName);
        tv_user_email = (TextView) header.findViewById(R.id.userEmail);
        tv_user_name.setText(guardian_name);
        tv_user_email.setText(email);

        Log.d(TAG, "Fetching user from Sqlite: " + guardian_name);


        tv_student_id = (TextView) findViewById(R.id.student_id);
        tv_student_name = (TextView) findViewById(R.id.student_name);
        tv_student_class = (TextView) findViewById(R.id.student_class);

//        getChildJson();

    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(AppConfig.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        Log.e(TAG, "Firebase reg id: " + regId);


    }


    public void getChildJson() {
//        String tag_string_req = "req_csrf";
        String tag_string_req = "req_data";
//
        pDialog.setMessage("Fetching data ...");
        showDialog();

        JsonArrayRequest req = new JsonArrayRequest(Request.Method.GET,
                AppConfig.URL_GET_S_INFO, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
//                        Log.i(TAG,"we are here");
                try {
                    hideDialog();
                    session.setLogin(true);
//                        JSONObject jObj = new JSONObject(response);
                    Log.i("tagconvertresponse", "[" + response.toString() + "]");
//                    JSONObject jObj = new JSONObject(response);
////
////                    JSONObject jsonObject = new JSONObject("student");
//                    JSONArray result = jObj.getJSONArray("student");
                    ArrayList<Students> listdata = new ArrayList<>();
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject jo = response.getJSONObject(i);
                        Students students = new Students();
                        students.setStudentId(jo.getString(AppConfig.TAG_SID_S));
                        students.setStudentName(jo.getString(AppConfig.TAG_SN_S));
                        students.setStudentClass(jo.getString(AppConfig.TAG_CID_S));

                        listdata.add(students);
//                        showStudent(listdata);
                    }
                } catch (JSONException e) {
                    hideDialog();
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideDialog();
                        Log.i("error", error.getMessage());
                    }
                }) {
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req, tag_string_req);


    }

//    private void showStudent(ArrayList<Students> listdata) {
//        Log.i(TAG, "we are here");
//        mRecyclerView = (RecyclerView) findViewById(R.id.cardlist_student);
//        mRecyclerView.setHasFixedSize(true);
//        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
//        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
//        mRecyclerView.setAdapter(new StudentsCardAdapter(listdata, new StudentsCardAdapter.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(View view, int position) {
//
//            }
//
//            @Override
//            public void onItemClickExpand(View view, int position) {
//
//            }
//        }
//
//        ));
//
//    }

    private void logoutUser() {
        session.setLogin(false);
        db.deleteUsers();
        // Launching the login activity
        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (!doubleBackToExitPressedOnce) {
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click BACK again to exit.", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            } else {
                finishAffinity();
                super.onBackPressed();
                return;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(AppConfig.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(AppConfig.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        Class fragmentClass = null;

        if (id == R.id.nav_student) {
            fragmentClass = StudentActivityFragment.class;

        } else if (id == R.id.nav_feedback) {
            fragmentClass = FeedbackActivityFragment.class;

        } /*else if (id == R.id.nav_manage) {

        } */ else if (id == R.id.nav_notice) {
            fragmentClass = ScrollableNoticeTabsActivity.class;

        } else if (id == R.id.nav_logout) {
            logoutUser();
        }
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (fragmentClass != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();


        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    public void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


}
