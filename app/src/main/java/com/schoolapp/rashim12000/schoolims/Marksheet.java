package com.schoolapp.rashim12000.schoolims;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Rashim12000 on 11/4/2017.
 */


public class Marksheet {
    String sn;
    String studnetName;
    String className;
    String examName;
    boolean hasFile;
    String fileName;
    String[] subjects;
    String[] obtMarks;



    public  Marksheet(){}

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Marksheet(String sn, String studnetName, String className, String examName, boolean hasFile, String fileName, String[] subjects, String[] obtMarks) {

        this.sn = sn;
        this.studnetName = studnetName;
        this.className = className;
        this.examName = examName;
        this.hasFile = hasFile;
        this.fileName = fileName;
        this.subjects = subjects;
        this.obtMarks = obtMarks;
    }

    public String getStudnetName() {
        return studnetName;
    }

    public void setStudnetName(String studnetName) {
        this.studnetName = studnetName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public boolean isHasFile() {
        return hasFile;
    }

    public void setHasFile(boolean hasFile) {
        this.hasFile = hasFile;
    }

    public String[] getSubjects() {
        return subjects;
    }

    public void setSubjects(String[] subjects) {
        this.subjects = subjects;
    }

    public String[] getObtMarks() {
        return obtMarks;
    }

    public void setObtMarks(String[] obtMarks) {
        this.obtMarks = obtMarks;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
