package com.schoolapp.rashim12000.schoolims;

import android.widget.ImageView;

/**
 * Created by Rashim12000 on 10/31/2017.
 */

public class NoticeStudent {
    String noticeTitle;
    String noticeBody;
    String noticeStudent;
    String noticeDate;
    ImageView noticeImage;

    public NoticeStudent(){

    }

    public NoticeStudent(String noticeTitle, String noticeBody, String noticeStudent, String noticeDate, ImageView noticeImage) {
        this.noticeTitle = noticeTitle;
        this.noticeBody = noticeBody;
        this.noticeStudent = noticeStudent;
        this.noticeDate = noticeDate;
        this.noticeImage = noticeImage;
    }

    public String getNoticeTitle() {
        return noticeTitle;
    }

    public void setNoticeTitle(String noticeTitle) {
        this.noticeTitle = noticeTitle;
    }

    public String getNoticeBody() {
        return noticeBody;
    }

    public void setNoticeBody(String noticeBody) {
        this.noticeBody = noticeBody;
    }

    public String getNoticeStudent() {
        return noticeStudent;
    }

    public void setNoticeStudent(String noticeStudent) {
        this.noticeStudent = noticeStudent;
    }

    public String getNoticeDate() {
        return noticeDate;
    }

    public void setNoticeDate(String noticeDate) {
        this.noticeDate = noticeDate;
    }

    public ImageView getNoticeImage() {
        return noticeImage;
    }

    public void setNoticeImage(ImageView noticeImage) {
        this.noticeImage = noticeImage;
    }
}
